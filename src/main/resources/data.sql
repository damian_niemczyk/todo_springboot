insert into todo(id, username, description, target_date, is_done)
values(10001, 'damian', 'learn java', sysdate(), false);

insert into todo(id, username, description, target_date, is_done)
values(10002, 'damian', 'learn microservices', sysdate(), false);

insert into todo(id, username, description, target_date, is_done)
values(10003, 'damian', 'learn angular', sysdate(), false);
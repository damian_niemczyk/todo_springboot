package com.dniem.restfulwebservices.todo;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class TodoHardcodedService {

    private static List<Todo> todos = new ArrayList();
    private static Long idCounter = 0L;

    static {
        todos.add(new Todo(++idCounter, "damian", "learn java", new Date(), false));
        todos.add(new Todo(++idCounter, "damian", "learn microservices", new Date(), false));
        todos.add(new Todo(++idCounter, "damian", "learn angular", new Date(), false));
    }

    public List<Todo> findAll() {
        return todos;
    }

    public Todo save(Todo todo) {
        if (todo.getId() == -1 || todo.getId() == 0) {
            todo.setId(++idCounter);
            todos.add(todo);
        } else {
            deleteById(todo.getId());
            todos.add(todo);
        }
        return todo;
    }

    public Todo deleteById(long id) {
        Todo todo = findById(id);
        if (todo != null && todos.remove(todo)) {
            return todo;
        }
        return null;
    }

    public Todo findById (long id) {
        return todos.stream()
                .filter(x -> x.getId() == id)
                .findFirst().orElse(null);
    }
}
